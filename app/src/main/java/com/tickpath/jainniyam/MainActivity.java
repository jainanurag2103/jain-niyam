package com.tickpath.jainniyam;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent launchNextActivity;
                launchNextActivity = new Intent(getBaseContext(),TodaysNiyamActivity.class);
                startActivity(launchNextActivity);
                finish();
                //Toast.makeText(MainActivity.this, "Welcome to AVJ", Toast.LENGTH_SHORT).show();
            }
        }, 3000);
    }

}
